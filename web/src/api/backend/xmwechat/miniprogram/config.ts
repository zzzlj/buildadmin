import createAxios from '/@/utils/axios'

export function getConfig() {
    return createAxios({
        url: '/admin/xmwechat.miniprogram.Config/getConfig',
        method: 'get',
    })
}
export function saveConfig(type: string, data: anyObj) {
    return createAxios(
        {
            url: '/admin/xmwechat.miniprogram.Config/saveConfig',
            method: 'post',
            params: {
                type: type,
            },
            data: data,
        },
        {
            showSuccessMessage: true,
        }
    ) as ApiPromise
}
