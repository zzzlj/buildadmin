import createAxios from '/@/utils/axios'

export function getMenu() {
    return createAxios({
        url: '/admin/xmwechat.offiaccount.Menu/getMenu',
        method: 'get',
    })
}
export function saveMenu(data: anyObj) {
    return createAxios(
        {
            url: '/admin/xmwechat.offiaccount.Menu/saveMenu',
            method: 'post',
            data: data,
        },
        {
            showSuccessMessage: true,
        }
    ) as ApiPromise
}

export function getMenuContent(params: anyObj) {
    return createAxios(
        {
            url: '/admin/xmwechat.offiaccount.Material/getMenuContent',
            method: 'get',
            params: params,
        },
        {
            CancelDuplicateRequest: false,
        }
    ) as ApiPromise
}
