export default {
    Payment: 'payment（V3）',
    MchId: 'mchId',
    SecretKey: 'secretKey(V3)',
    NotifyUrl: 'notify url',
    Certificate: 'certificate',
    PrivateKey: 'privateKey',
    partnerPayment: 'partner payment',
    partnerMchId: 'partner mchId',
    partnerAppId: 'partner appId',
    partnerSubMchId: 'subMchId',
    partnerSecretKey: 'partner secretKey(V3)',
    partnerNotifyUrl: 'notifyUrl',
    partnerRefundsNotifyUrl: 'refunds notify url',
    partnerCertificate: 'partner certificate',
    partnerPrivateKey: 'partner privateKey',
}
