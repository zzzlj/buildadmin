export default {
    Offiaccount: 'Offiaccount',
    OffiName: 'name',
    OriginalId: 'original id',
    RedirectUrl: 'redirect url',
    OffiQrCode: 'qrCode',
    DeveloperInfo: 'developer info',
    AppId: 'appId',
    AppSecret: 'appSecret',
    Server: 'server',
    ServerUrl: 'server url',
    ServerToken: 'server token',
    EncodingAESKey: 'encodingAESKey',
    MessageEncryptionMethod: 'messageEncryption method',
    Plaintext: 'plaintext',
    Compatible: 'compatible',
    Security: 'security',
    Feature: 'feature',
    BusinessDomain: 'business domain',
    JsSecurityDomain: 'jsSecurity domain',
    WebsiteAuthDomain: 'websiteAuth domain',
    Copy: 'copy',
}
