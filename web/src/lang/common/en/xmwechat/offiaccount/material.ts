export default {
    id: 'ID',
    title: 'title',
    name: 'name',
    description: 'description',
    image: 'image',
    type: 'type',
    'type text': 'text',
    content: 'content',
    create_time: 'create time',
    update_time: 'update time',
    'quick Search Fields': 'ID',
}
