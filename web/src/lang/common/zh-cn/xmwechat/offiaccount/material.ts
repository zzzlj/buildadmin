export default {
    id: 'ID',
    title: '标题',
    name: '名称',
    description: '描述',
    image: '图片',
    type: '类型',
    'type text': '文本',
    content: '内容',
    create_time: '创建时间',
    update_time: '更新时间',
    'quick Search Fields': 'ID',
}
