export default {
    Payment: '支付配置',
    MchId: '商户号',
    SecretKey: '商户密钥Key(V3)',
    NotifyUrl: '支付回调地址',
    Certificate: '微信支付证书',
    PrivateKey: '微信支付证书密钥',
    partnerPayment: '支付配置(合作伙伴、服务商模式)',
    partnerMchId: '服务商商户号',
    partnerAppId: '服务商APPDID',
    partnerSubMchId: '子商户号',
    partnerSecretKey: '服务商密钥Key(V3)',
    partnerNotifyUrl: '支付回调地址',
    partnerRefundsNotifyUrl: '退款回调地址',
    partnerCertificate: '服务商微信支付证书',
    partnerPrivateKey: '服务商微信支付证书密钥',
}
