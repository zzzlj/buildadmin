export default {
    id: 'id',
    apps_code: 'apps_code',
    apps_name: 'apps_name',
    system_version: 'system_version',
    language: 'language',
    register_time: 'register_time',
    wechat_qrcode: 'wechat_qrcode',
    wechat_name: 'wechat_name',
    company_name: 'company_name',
    create_time: 'create_time',
    modify_time: 'modify_time',
    'quick Search Fields': 'id',
}
