export default {
    id: 'id',
    dict_type: 'dict_type',
    dict_code: 'dict_code',
    dict_value: 'dict_value',
    alias_name: 'alias_name',
    remark: 'remark',
    status: 'status',
    'status 1': 'status 1',
    create_time: 'create_time',
    modify_time: 'modify_time',
    'quick Search Fields': 'id',
}
