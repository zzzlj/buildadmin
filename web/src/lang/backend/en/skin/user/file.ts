export default {
    id: 'id',
    user_id: 'user_id',
    source: 'source',
    purpose: 'purpose',
    batch_no: 'batch_no',
    name: 'name',
    path: 'path',
    md5: 'md5',
    create_time: 'create_time',
    modify_time: 'modify_time',
    'quick Search Fields': 'id',
}
