export default {
    id: 'ID',
    dict_type: '字典类型',
    dict_code: '字典Code',
    dict_value: '字典键值',
    alias_name: '别称',
    remark: '备注',
    status: '状态',
    'status 1': '正常',
    create_time: '创建时间',
    modify_time: '修改时间',
    'quick Search Fields': 'ID',
}
