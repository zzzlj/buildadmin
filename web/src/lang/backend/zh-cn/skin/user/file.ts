export default {
    id: 'ID',
    user_id: '所有者（会员ID）',
    source: '来源渠道',
    purpose: '目的（用途）',
    batch_no: '上传批次号',
    name: '文件名称',
    path: '文件保存路径',
    md5: '文件的MD5值',
    create_time: '创建时间',
    modify_time: '修改时间',
    'quick Search Fields': 'ID',
}
