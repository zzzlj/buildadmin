export default {
    id: 'ID',
    apps_code: '产品编号',
    apps_name: '产品名称',
    system_version: '系统版本',
    language: '语言',
    register_time: '注册时间',
    wechat_qrcode: '二维码',
    wechat_name: '所属公众号',
    company_name: '公司名称',
    create_time: '创建时间',
    modify_time: '修改时间',
    'quick Search Fields': 'ID',
}
