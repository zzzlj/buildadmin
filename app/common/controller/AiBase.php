<?php

namespace app\common\controller;

use Throwable;
use think\exception\HttpResponseException;

class AiBase extends Api
{

    protected string $secret = "abcdefghijklmnopqrstuvwxyz";
    protected string $appKey = "a1234567890Z";
    protected string $ver = "1.0";
    protected string $format = "json";
    protected array $bodyData = [];
    protected array $queryData = [];
    /**
     * 无需鉴权的方法
     * @var array
     */
    protected array $noNeedPermission = [];

    
    /**
     * 初始化
     * @throws Throwable
     * @throws HttpResponseException
     */
    public function initialize(): void
    {
        parent::initialize();
        //记录日志
        $bodyData = Request()->getContent();
        $this->queryData = input('','');
        $reqLog = PHP_EOL.'bodyData:'.$bodyData.PHP_EOL.'queryData:'.var_export($this->queryData,true);
        trace($reqLog,'info');
        if($bodyData){
            $this->bodyData = json_decode($bodyData, true);
        }
        if (!action_in_arr($this->noNeedPermission)) {
            $this->checkSign(); //验签
        }
    }


    /**
     * 验签
     */
    public function checkSign(): void
    {
        $params = $this->queryData;
        if (empty($params)) {
            $this->error('参数错误', $params, 1020);
        }
        if (empty($params['serial'])) {
            $this->error('时间戳serial为空', $params, 1021);
        }
        $expireTime = $params['serial'] + 3600;
        if ($expireTime < time()) {
            //$this->error('时间戳serial超时', $params, 1026);
        }
        if (empty($params['sign'])) {
            $this->error('签名不能为空', $params, 105);
        }
        if (empty($params['appKey']) || $params['appKey'] != $this->appKey) {
            $this->error('无效的标识串', $params, 1027);
        }
        if (empty($params['ver']) || $params['ver'] != $this->ver){
            $this->error('无效的版本号', $params, 1028);
        }
        if (empty($params['format']) || $params['format'] != $this->format){
            $this->error('不支持的返回数据格式', $params, 1029);
        }
        ksort($params);
        $signVal = '';
        foreach ($params as $k => $v) {
            if (in_array($k, ['server', 'sign']))
                continue;
            $signVal .= urldecode($v);
        }
        $sign = md5($this->secret . $signVal);
        if ($sign != $params['sign']) {
            $params['sysSign'] = $sign;
            $this->error('验签失败!', $params, 1030);
        }
    }

}