<?php

namespace app\common\library\xmwechat;

use think\exception\HttpResponseException;
use think\facade\Config;
use think\Response;

class WechatService
{
    // 公众号配置
    protected array $offiAccountConfig;

    // 小程序配置
    protected array $miniProgramConfig;

    // 微信支付配置
    protected array $wechatPayConfig;

    // 微信支付服务商配置
    protected array $wechatPartnerPayConfig;

    public function __construct()
    {
        $config                       = config('xmwechat');
        $this->offiAccountConfig      = $config['offiAccount'];
        $this->miniProgramConfig      = $config['miniProgram'];
        $this->wechatPayConfig        = $config['payment'];
        $this->wechatPartnerPayConfig = $config['partnerPayment'];
    }


    /**
     * 返回 API 数据
     * @param string $msg 提示消息
     * @param mixed $data 返回数据
     * @param int $code 错误码
     * @param string|null $type 输出类型
     * @param array $header 发送的 header 信息
     * @param array $options Response 输出参数
     */
    public function result(string $msg, mixed $data = null, int $code = 0, string $type = null, array $header = [], array $options = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => request()->server('REQUEST_TIME'),
            'data' => $data,
        ];

        // 如果未设置类型则自动判断
        $type = $type ?: (request()->param(Config::get('route.var_jsonp_handler')) ? 'jsonp' : 'json');

        $code = 200;
        if (isset($header['statuscode'])) {
            $code = $header['statuscode'];
            unset($header['statuscode']);
        }

        $response = Response::create($result, $type, $code)->header($header)->options($options);
        throw new HttpResponseException($response);
    }


}