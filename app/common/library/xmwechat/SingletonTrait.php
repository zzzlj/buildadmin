<?php

namespace app\common\library\xmwechat;

trait SingletonTrait
{
    // 存放实例 私有静态变量
    protected static $instance;

    // 声明私有构造方法为了防止外部代码使用new来创建对象。
    private function __construct()
    {
    }

    // 私有化克隆方法 static
    private function __clone()
    {
    }

    // 重写__sleep方法，将返回置空，防止序列化反序列化获得新的对象
    public function __sleep()
    {
        return [];
    }

    // 防止被反序列化
    public function __wakeup()
    {
    }

    /**
     * @desc   公有化获取实例方法
     * @return static
     */
    public static function getInstance(): static
    {
        if (!isset(self::$instance)) {
            // 这里不能new self(),self和static区别
            self::$instance = new static;
        }
        return self::$instance;
    }
}