<?php

namespace app\common\model;

use think\model;

/**
 * SkinUser
 */
class SkinDevice extends Model
{
    // 表名
    protected $name = 'skin_device';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

}