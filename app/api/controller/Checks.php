<?php

namespace app\api\controller;

use app\common\controller\AiBase;
use Throwable;
use app\common\model\SkinReport;
use think\Facade\Db;
class Checks extends AiBase
{

    protected array $noNeedPermission = [];

    public function initialize(): void
    {
        parent::initialize();
    }


     /**
     * 检测结果
     */
    public function checkReulst(): void
    {
        $params = $this->bodyData;
        $checkId = $params['checkId']??'';
        if(!$checkId){
            $this->error('检测报告ID不能为空');
        }
        $userid = $params['userid']??'';
        $appsCode = $params['appsCode']??'';
        $isBluetooth = $params['isBluetooth']??''; //是否为绑定蓝牙数据
        $health = $params['health']??'';
        $healthLevel = $params['healthLevel']??'';
        $conclusion = $params['conclusion']??'';
        $suggest = $params['suggest']??'';
        $skinType = $params['skinType']??'';
        $checkStatus = $params['checkStatus']??0;
        $checkTime = $params['checkTime']??date('Y-m-d H:i:s');
        $items = $params['items']??[];
        //更新检测报告
        $setData = [
            'conclusion'=>$conclusion,
            'suggest'=>$suggest,
            'health_score'=>$health,
            'health_level'=>$healthLevel,
            'skin_type'=>$skinType,
            'status'=>intval($checkStatus),
            'check_time'=>$checkTime,
        ];
        if($isBluetooth){
            $setData = [
                'is_bind_blue'=>1
            ];
        }
        Db::name('skin_report')->where(['id'=>$checkId])->update($setData);
        //更新检测项
        $blueArr = ['SYA_B001','SYA_B002','SYA_B003'];
        foreach ($items as $rs) {
            $where = ['report_id'=>$checkId,'item_code'=>$rs['code']];
            $itemInfo = Db::name('skin_report_item')->where($where)->find();
            $inData = [
                'report_id'=>$checkId,
                'item_code'=>$rs['code'],
                'status'=>intval($rs['status']),
                'intro'=>$rs['intro'],
                'experts'=>$rs['experts'],
                'personal'=>$rs['personal'],
                'suggest'=>$rs['suggest'],
                'image_path'=>$rs['image'],
                'score_value'=>$rs['value'],
                'score_level'=>$rs['level'],
            ];
            //蓝牙数据
            if(in_array($rs['code'],$blueArr)){
                $inData = [
                    'status'=>intval($rs['status']),
                    'score_value'=>$rs['value'],
                    'score_level'=>$rs['level']
                ];
            }
            if($itemInfo){
                $inData['id'] = $itemInfo['id'];
            }
            $itemInfo = Db::name('skin_report_item')->save($inData);
        }
        $this->success('success',[],200);

    }


     /**
     * 待检测列表
     */
    public function files(): void
    {
        $params = $this->queryData;
        $pageNum = $params['pageNum']??1;
        $pageSize = $params['pageSize']??100;
        //查询条件
        $where = ['status'=>0,'delete_state'=>1];
        $total = Db::name("skin_report")->where($where)->count();
        $list = Db::name("skin_report")->field('id,user_id,report_no,apps_id,status')->where($where)->order('id desc')->page($pageNum,$pageSize)->select();
        $newList = [];
        $domain = request()->domain();
        foreach ($list as $rs) {
            $rows = [];
            $rows['checkId'] = $rs['id'];
            $rows['userid'] = $rs['user_id'];
            //获取文件列表
            $fileList = Db::name('skin_user_file')->where(['batch_no'=>$rs['id']])->field('name,path')->order('id desc')->select();
            $newFiles = [];
            foreach ($fileList as $fi) {
                $ff = [];
                $ff['filename'] = $fi['name'];
                $ff['downloadUrl'] = $fi['path']?$domain.$fi['path']:'';
                $newFiles[] = $ff;
            }
            $rows['files'] = $newFiles;
            $newList[] = $rows;
        }
        $data = [
            'total'=>$total,'page'=>$pageNum,'size'=>$pageSize,'pages'=>ceil($total/$pageSize),'rows'=>$newList
        ];
        $this->success('success', $data,200);
    }


     /**
     * 获取列表
     */
    public function lists(): void
    {
        $params = $this->queryData;
        $pageNum = $params['pageNum']??1;
        $pageSize = $params['pageSize']??10;
        $keywords = $params['keywords']??'';
        $userid = $params['userid']??'';
        $appsCode = $params['appsCode']??'';
        $startTime = $params['startTime']??'';
        $endTime = $params['endTime']??'';
        //查询条件
        $where = 'r.delete_state=0';
        if($userid){
            $where .= " and u.user_id = {$userid} ";
        }elseif($appsCode){
            $where .= " and d.apps_code = '{$appsCode}' ";
        }elseif($keywords){
            $where .= " and r.report_no = '{$keywords}' or u.phone = '{$keywords}' ";
        }
        if($startTime){
            $where .= " and r.check_time >= '{$startTime}' ";
        }
        if($endTime){
            $where .= " and r.check_time <= '{$endTime}' ";
        }
        $fields = 'r.*,u.phone,u.name as username,d.apps_code,d.apps_name,d.company_name';
        $total = Db::name("skin_report")
        ->alias("r")
        ->leftJoin("skin_user u","r.user_id = u.id")
        ->leftJoin("skin_device d","r.apps_id = d.id")
        ->where($where)
        ->count();
        $list = Db::name("skin_report")
        ->alias("r")
        ->field($fields)
        ->leftJoin("skin_user u","r.user_id = u.id")
        ->leftJoin("skin_device d","r.apps_id = d.id")
        ->where($where)
        ->order('r.id desc')
        ->page($pageNum,$pageSize)
        //->fetchSql(true)
        ->select();
        $newList = [];
        foreach ($list as $key => $item) {
            $rows = [];
            $rows['checkId'] = $item['id'];
            $rows['reportNo'] = $item['report_no'];
            $rows['userid'] = $item['user_id'];
            $rows['username'] = $item['username'];
            $rows['appsId'] = $item['apps_id'];
            $rows['appsName'] = $item['apps_name'];
            $rows['productCode'] = $item['apps_code'];
            $rows['productName'] = $item['company_name'];
            $rows['health'] = $item['health_level'];
            $rows['score'] = $item['health_score'];
            $rows['conclusion'] = $item['conclusion'];
            $rows['suggest'] = $item['suggest'];
            $rows['skinType'] = $item['skin_type'];
            $rows['status'] = $item['status'];
            $rows['checkTime'] = $item['check_time'];
            $rows['createTime'] = $item['create_time'];
            $rows['addTime'] = $item['create_time'];
            $rows['isBluetooth'] = $item['is_bind_blue'];
            $rows['reportImg'] = $item['report_img'];
            $rows['oldData'] = $item;
            $newList[] = $rows;
        }
        $data = [
            'total'=>$total,'page'=>$pageNum,'size'=>$pageSize,'pages'=>ceil($total/$pageSize),'rows'=>$newList
        ];
        $this->success('success', $data,200);
    }

    

     /**
     * 获取详情
     */
    public function info(): void
    {
        $params = $this->queryData;
        $checkId = $params['checkId']??'';
        $reportNo = $params['reportNo']??'';
        $phone = $params['phone']??'';
        // $qryType = $params['qryType']??'';
        if(!$checkId && !$reportNo && !$phone){
            $this->error('参数错误');
        }
        //查询条件
        $where = '1=1';
        if($checkId){
            $where .= " and r.id = {$checkId} ";
        }elseif($reportNo){
            $where .= " and r.report_no = '{$reportNo}'";
        }elseif($phone){
            $where .= " and u.phone = '{$phone}' ";
        }
        $fields = 'r.*,u.phone,u.name as username,d.apps_code,d.apps_name,d.company_name';
        $info = Db::name("skin_report")
        ->alias("r")
        ->field($fields)
        ->leftJoin("skin_user u","r.user_id = u.id")
        ->leftJoin("skin_device d","r.apps_id = d.id")
        ->where($where)
        ->order('r.id desc')
        ->find();
        if(!$info){
            $this->error('非法操作,数据不存在');
        }
        //返回数据
        $resData = [
            'checkId'=> $info['id'],
            'reportNo'=> $info['report_no'],
            'userid'=> $info['user_id'],
            'username'=> $info['username'],
            'appsName'=> $info['apps_name'],
            'productCode'=> $info['apps_code'],
            'productName'=> $info['company_name'],
            'health'=> $info['health_level'],
            'conclusion'=> $info['conclusion'],
            'suggest'=> $info['suggest'],
            'skinType'=> $info['skin_type'],
            'checkTime'=> $info['check_time'],
            'createTime'=> $info['create_time'],
            'checkData'=> [],
        ];
        //获取检测项
        $dictArr = Db::name('skin_dict')->where('status',1)->column('*','dict_code');
        $itemList = Db::name('skin_report_item')->where('report_id',$info['id'])->select();
        $checkData = [];
        foreach ($itemList as $item) {
            $rows = [];
            $dictInfo = $dictArr[$item['item_code']];
            $rows['itemModule'] = $dictInfo['remark'];
            $rows['itemName'] = $dictInfo['dict_value'];
            $rows['intro'] = $item['intro'];
            $rows['experts'] = $item['experts'];
            $rows['personal'] = $item['personal'];
            $rows['suggest'] = $item['suggest'];
            $rows['levelName'] = $item['score_level'];
            $rows['value'] = $item['score_value'];
            $rows['imageUrl'] = $item['image_url'];
            $checkData[] = $rows;
        }
        $resData['checkData'] = $checkData;
        $this->success('success', $resData,200);
    }

     /**
     * 重新检测
     */
    public function upStatus(): void
    {
        $params = $this->queryData;
        $checkId = $params['checkId']??'';
        if(!$checkId){
            $this->error('ID不能为空',$params);
        }
        $dateTime = date('Y-m-d H:i:s');
        $setData = [
            'status'=>0,
            'create_time'=>$dateTime,
            'modify_time'=>$dateTime,
        ];
        $res = Db::name('skin_report')->where('id',$checkId)->update($setData);
        if(!$res){
            $this->error('操作失败');
        }
        $this->success('success',$params,200);
    }


     /**
     * 删除
     */
    public function delete(): void
    {
        $params = $this->queryData;
        $checkId = $params['checkId']??'';
        if(!$checkId){
            $this->error('ID不能为空',$params);
        }
        $dateTime = date('Y-m-d H:i:s');
        $setData = [
            'delete_state'=>1,
            'modify_time'=>$dateTime,
        ];
        $res = Db::name('skin_report')->where('id',$checkId)->update($setData);
        if(!$res){
            $this->error('操作失败');
        }
        $this->success('success',$params,200);
    }


}