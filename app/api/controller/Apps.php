<?php

namespace app\api\controller;

use app\common\controller\AiBase;
use Throwable;
// use app\common\model\SkinDevice;
use think\Facade\Db;

class Apps extends AiBase
{

    protected array $noNeedPermission = [];

    public function initialize(): void
    {
        parent::initialize();
    }

   
    /**
     * 录入设备信息
     */
    public function enter(): void
    {
        $params = $this->bodyData;
        //enter登录
        $appsCode = $params['appsCode']??'';
        if(!$appsCode){
            $this->error('设备编号不能为空');
        }
        $info = Db::name('skin_device')->where(['apps_code'=>$appsCode])->find();
        $setData = [
            'apps_code'=>$appsCode,
            'apps_name'=>$$params['appsName']??'',
            'system_version'=>$$params['version']??$this->ver,
            'wechat_qrcode'=>$$params['wechatQrcode']??'',
            'wechat_name'=>$$params['wechatName']??'瑞莱仕健康',
            'company_name'=>$$params['companyName']??'瑞莱仕（深圳）健康管理有限公司',
        ];
        if($info){
            $setData['modify_time'] = date('Y-m-d H:i:s');
            Db::name('skin_device')->where(['id'=>$info['id']])->update($setData);
            $id = $info['id'];
        }else{
            $setData['register_time'] = date('Y-m-d H:i:s');
            $id = Db::name('skin_device')->insertGetId($setData);
        }
        $this->success('success', $id,200);
    }

     /**
     * 获取列表
     */
    public function lists(): void
    {
        $params = $this->queryData;
        $pageNum = $params['pageNum']??1;
        $pageSize = $params['pageSize']??10;
        $keywords = $params['keywords']??'';
        //查询条件
        $where = '1=1';
        if($keywords){
            $where .= " and (apps_code like '%{$keywords}%' or apps_name like '%{$keywords}%') ";
        }
        $total = Db::name('skin_device')->whereRaw($where)->count();
        $list = Db::name('skin_device')->whereRaw($where)->order('id desc')->page($pageNum,$pageSize)->select();
        $newList = [];
        foreach ($list as $key => $rs) {
            $rows = [];
            $rows['appsId'] = $rs['id'];
            $rows['appsName'] = $rs['apps_name'];
            $rows['productCode'] = $rs['apps_code'];
            $rows['productName'] = $rs['company_name'];
            $rows['version'] = $rs['system_version'];
            $rows['lastedIp'] = '';
            $rows['registerTime'] = $rs['register_time'];
            $rows['creatTime'] = $rs['create_time'];
            $rows['modifyTime'] = $rs['modify_time'];
            $newList[] = $rows;
        }
        $data = [
            'total'=>$total,'page'=>$pageNum,'size'=>$pageSize,'pages'=>ceil($total/$pageSize),'rows'=>$newList
        ];
        $this->success('success', $data,200);
    }


     /**
     * 获取详情
     */
    public function info(): void
    {
        $params = $this->queryData;
        $appsid = $params['appsid']??'';
        if(!$appsid){
            $this->error('参数错误',$params);
        }
        //查询条件
        $info = Db::name('skin_device')->where(['id'=>$appsid])->find();
        if(!$info){
            $this->error('设备不存在',$info);
        }
        $resInfo = [
            'appsId'=>$info['id'],
            'appsName'=>$info['apps_name'],
            'productCode'=>$info['apps_code'],
            'productName'=>$info['company_name'],
            'version'=>$info['system_version'],
            'lastedIp'=>'',
            'registerTime'=>$info['register_time'],
            'creatTime'=>$info['create_time'],
            'modifyTime'=>$info['modify_time'],
        ];
        $this->success('success',$resInfo,200);
    }


}