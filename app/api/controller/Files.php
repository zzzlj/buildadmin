<?php

namespace app\api\controller;

use app\common\controller\AiBase;
use Throwable;
use think\Facade\Db;
use ba\Random;
use think\Facade\Filesystem as uploadFile;

class Files extends AiBase
{

    protected array $noNeedPermission = [];

    public function initialize(): void
    {
        parent::initialize();
    }


    /**
     * 上传检测文件
     */
    public function upload(): void
    {
        $files = request()->file('files');
        $channel = $this->queryData['channel']?:'pfjcy';
        $userid = $this->queryData['userid']??'';
        $purpose = $this->queryData['purpose']??'';
        $appsId = $this->queryData['appsId']??'';
        $savePath = 'input/'.$userid.'/'.date('Ymd');
        if(!$appsId || !$userid){
            $this->error('参数错误');
        }
        if(!is_array($files) || count($files)<5){
            $this->error('请一次上传5张检测图');
        }
        try {
            $resData = [];
            validate(['image'=>'fileSize:10240|fileExt:jpg,png|fileMime:image/jpg,image/jpeg,image/png'])
            ->check($files);
            foreach($files as $file){
                $fileInfo = [];
                $fileName = $file->getOriginalName();
                $fileHash = $file->hash('md5'); //sha1
                $filePath = uploadFile::disk('public')->putFileAs($savePath, $file,$fileName);
                $fileInfo['filePath'] ='/storage/'.$filePath;
                $fileInfo['filename'] = $fileName;
                $fileInfo['fileHash'] = $fileHash;
                $fileInfo['downloadUrl'] = request()->domain().'/storage/'.$filePath;
                $resData[] = $fileInfo;
            }
        } catch (Throwable $e) {
            $this->error($e->getMessage());
        }
        //开启事物
        Db::startTrans();
        try {
            //创建报告记录
            $reportNo = date('Ymd').Random::build('numeric',3);
            $reportData = [
                'report_no'=>$reportNo,
                'apps_id'=>$appsId,
                'user_id'=>$userid,
            ];
            $reportId = Db::name('skin_report')->insertGetId($reportData);
            //创建文件记录
            $inData = [];
            foreach ($resData as $rs) {
                $row = [];
                $row['user_id'] = $userid;
                $row['source'] = $channel;
                $row['purpose'] = $purpose;
                $row['batch_no'] = $reportId;
                $row['name'] = $rs['filename'];
                $row['path'] = $rs['filePath'];
                $row['md5'] = $rs['fileHash'];
                $inData[] = $row;
            }
            Db::name('skin_user_file')->limit(5)->insertAll($inData);
            Db::commit();
        } catch (Throwable $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        $this->success('success', $resData);
    }


    /**
     * 上传检测报告
     */
    public function uploadOne(): void
    {
        $files = request()->file();
        $filesOne = $files['file']??'';
        $reportId = $this->queryData['reportId']??'';
        $savePath = 'report/'.date('Ymd').'/'.$reportId;
        if(!$reportId || !$filesOne){
            $this->error('参数错误');
        }
       try {
            validate(['image'=>'fileSize:10240|fileExt:jpg,png|fileMime:image/jpg,image/jpeg,image/png'])->check($files);
            $fileName = $filesOne->getOriginalName();
            $filePath = uploadFile::disk('public')->putFileAs($savePath, $filesOne,$fileName);
            $reprotPath ='/storage/'.$filePath;
            $dateTime = date('Y-m-d H:i:s');
            $setData = [
                'report_img'=>$reprotPath,
                'modify_time'=>$dateTime,
            ];
            $res = Db::name('skin_report')->where('id',$reportId)->update($setData);
            if(!$res){
                $this->error('操作失败');
            }
        } catch (Throwable $e) {
            $this->error($e->getMessage());
        }
        $this->success('success', $setData);
    }



}