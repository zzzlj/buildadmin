<?php

namespace app\api\controller;

use app\common\controller\AiBase;
use Throwable;
use app\common\model\SkinUser;

class Users extends AiBase
{

    protected array $noNeedPermission = ['signin'];

    public function initialize(): void
    {
        parent::initialize();
    }

   
    /**
     * 登录注册
     */
    public function signin(): void
    {
        $params = $this->bodyData;
        $params['mode'] = $params['mode'] ??'';
        if(!$params['mode']){
            $this->error('登录方式不能为空',$params);
        }
        //扫码登录
        if($params['mode']=='wechat'){
            $userInfo = SkinUser::where('',$params[''])->find();
            $this->success('success',$userInfo,200);
        }
        //enter登录
        $needFidels = ['appsid'=>'设备id','name'=>'姓名','sex'=>'性别','phone'=>'手机号','age'=>'年龄'];
        foreach ($needFidels as $key => $value) {
            $params[$key] = $params[$key]??'';
            if(!$params[$key] || !trim($params[$key])){
                $this->error("参数错误, {$value}不能为空!",$params);
            }
        }
        $params['apps_id'] = $params['appsId'];
        $userInfo = SkinUser::where('phone',$params['phone'])->field('id')->find();
        if($userInfo){
           SkinUser::update($params,['id'=>$userInfo['id']]);
        }else{
           $userInfo = SkinUser::create($params);
        }
        $this->success('success', $userInfo->id,200);
    }


    /**
     * 获取微信二维码
     * @throws Throwable
     */
    public function getQrcode(): void
    {
        $data = [];
        $this->success('success', $data,200);
    }


     /**
     * 获取会员列表
     */
    public function lists(): void
    {
        $params = $this->queryData;
        $appsid = $params['appsId']??'';
        if(!$appsid){
            $this->error('设备ID不能为空',$params);
        }
        $pageNum = $params['pageNum']??1;
        $pageSize = $params['pageSize']??10;
        $keywords = $params['keywords']??'';
        //查询条件
        $where = " apps_id={$appsid} ";
        if(is_numeric($keywords)){
            $where .= " and phone like '%{$keywords}%' ";
        }elseif($keywords){
            $where .= " and name like '%{$keywords}%' ";
        }
        $total = SkinUser::whereRaw($where)->count();
        $list = SkinUser::whereRaw($where)->order('id desc')->page($pageNum,$pageSize)->select();
        $newList = [];
        foreach ($list as $rs) {
            $rows = [];
            $rows['userid'] = $rs['id'];
            $rows['name'] = $rs['name'];
            $rows['avatar'] = $rs['avatar']?:'';
            $rows['phone'] = $rs['phone'];
            $rows['email'] = $rs['email'];
            $rows['sex'] = $rs['sex'];
            $newList[] = $rows;
        }
        $data = [
            'total'=>$total,'page'=>$pageNum,'size'=>$pageSize,'pages'=>ceil($total/$pageSize),'rows'=>$newList
        ];
        $this->success('success', $data,200);
    }


    /**
     * 修改用户信息
     */
    public function editinfo(): void
    {
        $params = $this->bodyData;
        $params['userid'] = $params['userid']??'';
        if(!$params['userid']){
            $this->error('用户ID不能为空',$params);
        }
        $userInfo = SkinUser::where('id',$params['userid'])->find();
        if(!$userInfo){
            $this->error('用户不存在',$params);
        }
        SkinUser::update($params,['id'=>$userInfo['id']]);
        foreach ($userInfo as $key => $value) {
            $userInfo[$key] = isset($params[$key])?$params[$key]:$userInfo[$key];
        }
        $this->success('success', $userInfo,200);
    }


     /**
     * 获取会员详情
     */
    public function info(): void
    {
        $params = $this->queryData;
        $appsid = $params['appsid']??'';
        if(!$appsid){
            $this->error('设备ID不能为空',$params);
        }
        $userid = $params['userid']??'';
        $openid = $params['openid']??'';
        $phone = $params['phone']??'';
        if(!$userid && !$openid && !$phone){
            $this->error('参数错误');
        }
        $where = "apps_id={$appsid}";
        if($userid){
            $where .= " and id={$userid} ";
        }
        if($openid){
            $where .= " and openid='{$openid}' ";
        }
        if($phone){
            $where .= " and phone='{$phone}' ";
        }
        //查询条件
        $userInfo = SkinUser::whereRaw($where)->find();
        if(empty($userInfo)){
            $this->error('用户不存在');
        }
        $userInfo['userid'] = $userInfo['id'];
        $this->success('success',$userInfo,200);
    }

    
    /**
     * 删除用户
     */
    public function delete(){
        $params = $this->queryData;
        $userid = $params['userid']??'';
        if(!$userid){
            $this->error('参数错误');
        }
        $userInfo = SkinUser::find($userid);
        $userInfo->status=0;
        $res = $userInfo->save();
        if(!$res){
            $this->error('操作失败');
        }
        $this->success('操作成功',$res,200);
    }


}