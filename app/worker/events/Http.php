<?php

namespace app\worker\events;

use ba\Filesystem;
use Workerman\Timer;
use Workerman\Worker;
use app\worker\library\Monitor;
use think\db\exception\PDOException;
use app\worker\library\WorkerHttpApp;
use Workerman\Protocols\Http\Request;
use Workerman\Protocols\Http\Response;
use Workerman\Connection\TcpConnection;

/**
 * http的回调类
 * 实现了静态文件访问和API访问
 */
class Http
{

    /**
     * 文件监听配置
     */
    protected static array $monitorConfig = [];

    /**
     * 等待响应的请求计数
     */
    protected static int $waitResponseCount = 0;

    /**
     * WorkerMan Http APP基础类
     */
    protected WorkerHttpApp $app;

    /**
     * 构造函数
     * 此函数内不能初始化资源(MySQL连接、Redis连接、Memcache连接等，比如不能使用Db)
     */
    public function __construct()
    {
    }

    /**
     * Worker子进程启动时的回调函数，每个子进程启动时都会执行。
     */
    public function onWorkerStart(Worker $worker): void
    {
        $this->app         = new WorkerHttpApp(root_path());
        $this->app->worker = $worker;
        $this->app->initialize();
        self::$monitorConfig = $this->app->config->get('worker_monitor');

        try {
            $this->app->db->execute("SELECT 1");
        } catch (PDOException) {
        }

        if (0 == $worker->id) {
            new Monitor(self::$monitorConfig);
        }
    }

    /**
     * 当客户端通过连接发来数据时(WorkerMan收到数据时)触发的回调函数
     */
    public function onMessage(TcpConnection $connection, Request $request): void
    {
        $this->app->init($connection, $request);

        $path = $request->path() ?: '/';
        $file = Filesystem::fsFit(public_path() . urldecode($path));

        if (!is_file($file)) {
            // 访问控制器

            self::$waitResponseCount++;
            if (self::$monitorConfig['soft_reboot'] && !Monitor::isPaused()) {
                Monitor::pause();
            }

            // 避免输出到命令行窗口
            while (ob_get_level() > 1) {
                ob_end_clean();
            }

            ob_start();

            $http     = $this->app->http;
            $response = $http->run();
            $content  = ob_get_clean();

            ob_start();
            $response->send();
            $this->app->http->end($response);
            $content .= ob_get_clean() ?: '';

            $connection->send(new Response($response->getCode(), $response->getHeader(), $content));

            self::$waitResponseCount--;
            if (self::$waitResponseCount <= 0 && self::$monitorConfig['soft_reboot']) {
                // 隔一次时间间隔再启动检测
                Timer::add(self::$monitorConfig['interval'], function () {
                    if (Monitor::isPaused()) {
                        Monitor::resume();
                    }
                }, [], false);
            }
        } else {
            // 访问静态文件

            // 文件未修改则返回304
            if (!empty($ifModifiedSince = $request->header('if-modified-since'))) {
                $modifiedTime = date('D, d M Y H:i:s', filemtime($file)) . ' ' . date_default_timezone_get();
                if ($modifiedTime === $ifModifiedSince) {
                    $connection->send(new Response(304));
                    return;
                }
            }

            // 文件修改过或没有 if-modified-since 头则发送文件
            $response = (new Response())->withFile($file);
            $connection->send($response);
        }
    }

    /**
     * 当客户端的连接上发生错误时触发。
     */
    public function onError(TcpConnection $connection, $code, $msg)
    {
    }
}