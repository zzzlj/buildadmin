<?php

namespace app\admin\controller\xmwechat\miniprogram;

use app\common\controller\Backend;

class Config extends Backend
{
    static string $configFile = 'xmwechat.php';

    protected array $xmwechat;

    public function initialize(): void
    {
        parent::initialize();
        $this->xmwechat = config('xmwechat');
    }

    /**
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function getConfig(): void
    {
        $miniProgram = $this->xmwechat['miniProgram'];
        $this->success('', [
            'miniprogram' => $miniProgram,
        ]);
    }

    /**
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function saveConfig(): void
    {
        $type = $this->request->get('type', '');
        $data = $this->request->post();
        if (!$type) {
            $this->error(__('Parameter error'));
        }
        $xmwechat      = $this->xmwechat;
        $configPath    = config_path() . self::$configFile;
        $configContent = @file_get_contents($configPath);
        if (!isset($xmwechat['miniProgram']) || !is_array($xmwechat['miniProgram'])) {
            $this->error(__('Parameter error'));
        }
        $miniProgram = $xmwechat['miniProgram'];
        if ($type == 'miniprogram') {
            $qr_code       = str_replace('/', '\/', $miniProgram['qr_code']);
            $configContent = preg_replace("/'name'(\s+)=>(\s+)'{$miniProgram['name']}'/", "'name'\$1=>\$2'{$data['name']}'", $configContent);
            $configContent = preg_replace("/'original_id'(\s+)=>(\s+)'{$miniProgram['original_id']}'/", "'original_id'\$1=>\$2'{$data['original_id']}'", $configContent);
            $configContent = preg_replace("/'qr_code'(\s+)=>(\s+)'{$qr_code}'/", "'qr_code'\$1=>\$2'{$data['qr_code']}'", $configContent);
        } elseif ($type == 'developerInfo') {
            $configContent = preg_replace("/'app_id'(\s+)=>(\s+)'{$miniProgram['app_id']}'/", "'app_id'\$1=>\$2'{$data['app_id']}'", $configContent);
            $configContent = preg_replace("/'secret'(\s+)=>(\s+)'{$miniProgram['secret']}'/", "'secret'\$1=>\$2'{$data['secret']}'", $configContent);
        } elseif ($type == 'messagePush') {
            $server_url    = str_replace('/', '\/', $miniProgram['server_url']);
            $configContent = preg_replace("/'server_url'(\s+)=>(\s+)'{$server_url}'/", "'server_url'\$1=>\$2'{$data['server_url']}'", $configContent);
            $configContent = preg_replace("/'server_token'(\s+)=>(\s+)'{$miniProgram['server_token']}'/", "'server_token'\$1=>\$2'{$data['server_token']}'", $configContent);
            $configContent = preg_replace("/'encoding_aes_key'(\s+)=>(\s+)'{$miniProgram['encoding_aes_key']}'/", "'encoding_aes_key'\$1=>\$2'{$data['encoding_aes_key']}'", $configContent);
            $configContent = preg_replace("/'message_encryption_method'(\s+)=>(\s+)'{$miniProgram['message_encryption_method']}'/", "'message_encryption_method'\$1=>\$2'{$data['message_encryption_method']}'", $configContent);
        } else {
            $this->error(__('Parameter error'));
        }
        $result = @file_put_contents($configPath, $configContent);
        if (!$result) {
            $this->error(__('Configuration write failed: %s', ['config/' . self::$configFile]));
        }
        $this->success();
    }

}