<?php

namespace app\admin\controller\xmwechat\payment;

use app\common\controller\Backend;

class Config extends Backend
{
    static string $configFile = 'xmwechat.php';

    protected array $xmwechat;

    public function initialize(): void
    {
        parent::initialize();
        $this->xmwechat = config('xmwechat');
    }

    /**
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function getConfig(): void
    {
        $this->success('', [
            'payment'        => $this->xmwechat['payment'],
            'partnerPayment' => $this->xmwechat['partnerPayment'],
        ]);
    }

    /**
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function saveConfig(): void
    {
        $type = $this->request->get('type', '');
        $data = $this->request->post();
        if (!$type) {
            $this->error(__('Parameter error'));
        }
        $xmwechat      = $this->xmwechat;
        $configPath    = config_path() . self::$configFile;
        $configContent = @file_get_contents($configPath);
        if ($type == 'payment') {
            if (!isset($xmwechat['payment']) || !is_array($xmwechat['payment'])) {
                $this->error(__('Parameter error'));
            }
            $notify_url    = str_replace('/', '\/', $xmwechat['payment']['notify_url']);
            $certificate   = str_replace('/', '\/', $xmwechat['payment']['certificate']);
            $private_key   = str_replace('/', '\/', $xmwechat['payment']['private_key']);
            $configContent = preg_replace("/'mch_id'(\s+)=>(\s+)'{$xmwechat['payment']['mch_id']}'/", "'mch_id'\$1=>\$2'{$data['mch_id']}'", $configContent);
            $configContent = preg_replace("/'secret_key'(\s+)=>(\s+)'{$xmwechat['payment']['secret_key']}'/", "'secret_key'\$1=>\$2'{$data['secret_key']}'", $configContent);
            $configContent = preg_replace("/'notify_url'(\s+)=>(\s+)'{$notify_url}'/", "'notify_url'\$1=>\$2'{$data['notify_url']}'", $configContent);
            $configContent = preg_replace("/'certificate'(\s+)=>(\s+)'{$certificate}'/", "'certificate'\$1=>\$2'{$data['certificate']}'", $configContent);
            $configContent = preg_replace("/'private_key'(\s+)=>(\s+)'{$private_key}'/", "'private_key'\$1=>\$2'{$data['private_key']}'", $configContent);
            $result        = @file_put_contents($configPath, $configContent);
        } elseif ($type == 'partnerPayment') {
            if (!isset($xmwechat['partnerPayment']) || !is_array($xmwechat['partnerPayment'])) {
                $this->error(__('Parameter error'));
            }
            $refunds_notify_url = str_replace('/', '\/', $xmwechat['partnerPayment']['refunds_notify_url']);
            $notify_url         = str_replace('/', '\/', $xmwechat['partnerPayment']['notify_url']);
            $certificate        = str_replace('/', '\/', $xmwechat['partnerPayment']['certificate']);
            $private_key        = str_replace('/', '\/', $xmwechat['partnerPayment']['private_key']);
            $configContent      = preg_replace("/'refunds_notify_url'(\s+)=>(\s+)'{$refunds_notify_url}'/", "'refunds_notify_url'\$1=>\$2'{$data['refunds_notify_url']}'", $configContent);
            $configContent      = preg_replace("/'notify_url'(\s+)=>(\s+)'{$notify_url}'/", "'notify_url'\$1=>\$2'{$data['notify_url']}'", $configContent);
            $configContent      = preg_replace("/'certificate'(\s+)=>(\s+)'{$certificate}'/", "'certificate'\$1=>\$2'{$data['certificate']}'", $configContent);
            $configContent      = preg_replace("/'private_key'(\s+)=>(\s+)'{$private_key}'/", "'private_key'\$1=>\$2'{$data['private_key']}'", $configContent);
            $configContent      = preg_replace("/'secret_key'(\s+)=>(\s+)'{$xmwechat['partnerPayment']['secret_key']}'/", "'secret_key'\$1=>\$2'{$data['secret_key']}'", $configContent);
            $configContent      = preg_replace("/'sp_mchid'(\s+)=>(\s+)'{$xmwechat['partnerPayment']['sp_mchid']}'/", "'sp_mchid'\$1=>\$2'{$data['sp_mchid']}'", $configContent);
            $configContent      = preg_replace("/'sp_appid'(\s+)=>(\s+)'{$xmwechat['partnerPayment']['sp_appid']}'/", "'sp_appid'\$1=>\$2'{$data['sp_appid']}'", $configContent);
            $configContent      = preg_replace("/'sub_mchid'(\s+)=>(\s+)'{$xmwechat['partnerPayment']['sub_mchid']}'/", "'sub_mchid'\$1=>\$2'{$data['sub_mchid']}'", $configContent);
            $configContent      = preg_replace("/'secret_key'(\s+)=>(\s+)'{$xmwechat['partnerPayment']['secret_key']}'/", "'secret_key'\$1=>\$2'{$data['secret_key']}'", $configContent);
            $result             = @file_put_contents($configPath, $configContent);
        }
        if (!$result) {
            $this->error(__('Configuration write failed: %s', ['config/' . self::$configFile]));
        }
        $this->success();
    }

}