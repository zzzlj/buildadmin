<?php

namespace app\admin\controller\xmwechat\offiaccount;

use app\common\controller\Backend;
use Throwable;

/**
 * 公众号自动回复
 */
class Autoreply extends Backend
{
    /**
     * Autoreply模型对象
     * @var object
     * @phpstan-var \app\admin\model\xmwechat\offiaccount\Autoreply
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'create_time', 'update_time'];

    protected string|array $quickSearchField = ['keyword'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\xmwechat\offiaccount\Autoreply;
        $this->request->filter('trim,htmlspecialchars');
    }

    public function add(): void
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (!$data) {
                $this->error(__('Parameter %s can not be empty', ['']));
            }

            $data = $this->excludeFields($data);
            if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                $data[$this->dataLimitField] = $this->auth->id;
            }

            if ($data['type'] == 'follow' || $data['type'] == 'default') {
                $data['type'] == 'follow' ? $replyType = '关注回复' : $replyType = '默认回复';
                $reply = $this->model->where('type', $data['type'])->find();
                if (!empty($reply)) {
                    $this->error('当前已设置' . $replyType . '，请前往修改');
                }
            }

            $result = false;
            $this->model->startTrans();
            try {
                // 模型验证
                if ($this->modelValidate) {
                    $validate = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                    if (class_exists($validate)) {
                        $validate = new $validate;
                        if ($this->modelSceneValidate) $validate->scene('add');
                        $validate->check($data);
                    }
                }
                $result = $this->model->save($data);
                $this->model->commit();
            } catch (Throwable $e) {
                $this->model->rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success(__('Added successfully'));
            } else {
                $this->error(__('No rows were added'));
            }
        }

        $this->error(__('Parameter error'));
    }

    public function edit(): void
    {
        $id  = $this->request->param($this->model->getPk());
        $row = $this->model->find($id);
        if (!$row) {
            $this->error(__('Record not found'));
        }

        $dataLimitAdminIds = $this->getDataLimitAdminIds();
        if ($dataLimitAdminIds && !in_array($row[$this->dataLimitField], $dataLimitAdminIds)) {
            $this->error(__('You have no permission'));
        }

        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (!$data) {
                $this->error(__('Parameter %s can not be empty', ['']));
            }

            $data = $this->excludeFields($data);
            if ($data['type'] == 'follow' || $data['type'] == 'default') {
                $data['type'] == 'follow' ? $replyType = '关注回复' : $replyType = '默认回复';
                $reply = $this->model->where('type', $data['type'])->where('id', '<>', $row['id'])->find();
                if (!empty($reply)) {
                    $this->error('当前已设置' . $replyType . '，请前往修改');
                }
            }

            $result = false;
            $this->model->startTrans();
            try {
                // 模型验证
                if ($this->modelValidate) {
                    $validate = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                    if (class_exists($validate)) {
                        $validate = new $validate;
                        if ($this->modelSceneValidate) $validate->scene('edit');
                        $validate->check($data);
                    }
                }
                $result = $row->save($data);
                $this->model->commit();
            } catch (Throwable $e) {
                $this->model->rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success(__('Update successful'));
            } else {
                $this->error(__('No rows updated'));
            }
        }
        $this->success('', [
            'row' => $row
        ]);
    }

}