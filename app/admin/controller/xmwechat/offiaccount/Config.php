<?php

namespace app\admin\controller\xmwechat\offiaccount;

use app\common\controller\Backend;

class Config extends Backend
{
    static string $configFile = 'xmwechat.php';

    protected array $xmwechat;

    public function initialize(): void
    {
        parent::initialize();
        $this->xmwechat = config('xmwechat');
    }

    /**
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function getConfig(): void
    {
        $offiAccount = $this->xmwechat['offiAccount'];
        $this->success('', [
            'offiaccount' => $offiAccount,
        ]);
    }

    /**
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function saveConfig(): void
    {
        $type = $this->request->get('type', '');
        $data = $this->request->post();
        if (!$type) {
            $this->error(__('Parameter error'));
        }
        $xmwechat      = $this->xmwechat;
        $configPath    = config_path() . self::$configFile;
        $configContent = @file_get_contents($configPath);
        if (!isset($xmwechat['offiAccount']) || !is_array($xmwechat['offiAccount'])) {
            $this->error(__('Parameter error'));
        }
        $offiAccount = $xmwechat['offiAccount'];
        if ($type == 'offiaccount') {
            $offi_qr_code  = str_replace('/', '\/', $offiAccount['offi_qr_code']);
            $configContent = preg_replace("/'offi_name'(\s+)=>(\s+)'{$offiAccount['offi_name']}'/", "'offi_name'\$1=>\$2'{$data['offi_name']}'", $configContent);
            $configContent = preg_replace("/'original_id'(\s+)=>(\s+)'{$offiAccount['original_id']}'/", "'original_id'\$1=>\$2'{$data['original_id']}'", $configContent);
            $configContent = preg_replace("/'offi_qr_code'(\s+)=>(\s+)'{$offi_qr_code}'/", "'offi_qr_code'\$1=>\$2'{$data['offi_qr_code']}'", $configContent);
        } elseif ($type == 'developerInfo') {
            $redirect_url  = str_replace('/', '\/', $offiAccount['redirect_url']);
            $configContent = preg_replace("/'app_id'(\s+)=>(\s+)'{$offiAccount['app_id']}'/", "'app_id'\$1=>\$2'{$data['app_id']}'", $configContent);
            $configContent = preg_replace("/'secret'(\s+)=>(\s+)'{$offiAccount['secret']}'/", "'secret'\$1=>\$2'{$data['secret']}'", $configContent);
            $configContent = preg_replace("/'redirect_url'(\s+)=>(\s+)'{$redirect_url}'/", "'redirect_url'\$1=>\$2'{$data['redirect_url']}'", $configContent);
        } elseif ($type == 'server') {
            $server_url    = str_replace('/', '\/', $offiAccount['server_url']);
            $configContent = preg_replace("/'server_url'(\s+)=>(\s+)'{$server_url}'/", "'server_url'\$1=>\$2'{$data['server_url']}'", $configContent);
            $configContent = preg_replace("/'server_token'(\s+)=>(\s+)'{$offiAccount['server_token']}'/", "'server_token'\$1=>\$2'{$data['server_token']}'", $configContent);
            $configContent = preg_replace("/'encoding_aes_key'(\s+)=>(\s+)'{$offiAccount['encoding_aes_key']}'/", "'encoding_aes_key'\$1=>\$2'{$data['encoding_aes_key']}'", $configContent);
            $configContent = preg_replace("/'message_encryption_method'(\s+)=>(\s+)'{$offiAccount['message_encryption_method']}'/", "'message_encryption_method'\$1=>\$2'{$data['message_encryption_method']}'", $configContent);
        } else {
            $this->error(__('Parameter error'));
        }
        $result = @file_put_contents($configPath, $configContent);
        if (!$result) {
            $this->error(__('Configuration write failed: %s', ['config/' . self::$configFile]));
        }
        $this->success();
    }

}