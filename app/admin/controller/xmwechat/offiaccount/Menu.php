<?php

namespace app\admin\controller\xmwechat\offiaccount;

use app\common\controller\Backend;
use app\common\library\xmwechat\offiaccount\MenuService;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Menu extends Backend
{
    protected array $menuType = ['article_id', 'media_id', 'click', 'view', 'miniprogram'];

    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * 获取菜单
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function getMenu(): void
    {
        try {
            $data = MenuService::getInstance()->getMenu();
            $this->success('success', $data);
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * 保存并发布菜单
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function saveMenu(): void
    {
        $params = $this->request->param();
        $result = false;
        try {
            $this->checkMenu($params['button']);
            $result = MenuService::getInstance()->saveAndPublish($params);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        if ($result) {
            $this->success('发布成功');
        } else {
            $this->error('发布失败');
        }
    }

    /**
     * 一级菜单校验
     * @param $menuParams
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function checkMenu($menuParams): void
    {
        if (empty($menuParams) || !is_array($menuParams)) {
            $this->result(0, "请正确设置菜单格式");
        }
        if (count($menuParams) > 3) {
            $this->result(0, "一级菜单最多设置3个");
        }
        foreach ($menuParams as $item) {
            if (!is_array($item)) {
                $this->result(0, "一级菜单数据格式有误");
            }
            if (empty($item['name'])) {
                $this->result(0, "请输入一级菜单名称");
            }
            if (isset($item['sub_button'])) {
                if (!empty($item['sub_button'])) {
                    $this->checkSubMenu($item['sub_button']);
                }
            } else {
                if (empty($item['type'])) {
                    $this->result(0, "一级菜单未选择菜单类型");
                }
                if (!in_array($item['type'], $this->menuType)) {
                    $this->result(0, "一级菜单类型错误");
                }
                $this->checkMenuType($item);
            }
        }
    }

    /**
     * 二级菜单校验
     * @param $subMenuParams
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function checkSubMenu($subMenuParams): void
    {
        if (!is_array($subMenuParams)) {
            $this->result(0, "二级菜单数据格式有误");
        }
        if (count($subMenuParams) > 5) {
            $this->result(0, "二级菜单最多设置5个");
        }
        foreach ($subMenuParams as $subItem) {
            if (!is_array($subItem)) {
                $this->result(0, "二级菜单数据格式有误");
            }
            if (empty($subItem['name'])) {
                $this->result(0, "请输入二级菜单名称");
            }
            if (empty($subItem['type']) || !in_array($subItem['type'], $this->menuType)) {
                $this->result(0, "二级菜单类型有误");
            }
            $this->checkMenuType($subItem);
        }
    }

    /**
     * 检查菜单类型
     * @param $item
     * @return void
     * @author jsjxsz <QQ:1365962177>
     */
    public function checkMenuType($item): void
    {
        switch ($item['type']) {
            case 'click':
                if (empty($item['key'])) {
                    $this->result(0, "菜单KEY不能为空");
                }
                break;
            case 'view':
                if (empty($item['url'])) {
                    $this->result(0, "请输入网页链接");
                }
                break;
            case 'miniprogram':
                if (empty($item['url'])) {
                    $this->result(0, "请输入网页链接");
                }
                if (empty($item['appid'])) {
                    $this->result(0, "请输入appid");
                }
                if (empty($item['pagepath'])) {
                    $this->result(0, "请输入小程序路径");
                }
                break;
        }
    }

}