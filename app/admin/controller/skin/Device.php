<?php

namespace app\admin\controller\skin;

use app\common\controller\Backend;

/**
 * 皮肤检测仪
 */
class Device extends Backend
{
    /**
     * Device模型对象
     * @var object
     * @phpstan-var \app\admin\model\skin\Device
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'create_time'];

    protected string|array $quickSearchField = ['id'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\skin\Device;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */
}