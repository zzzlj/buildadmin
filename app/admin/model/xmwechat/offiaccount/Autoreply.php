<?php

namespace app\admin\model\xmwechat\offiaccount;

use think\Model;

/**
 * Autoreply
 */
class Autoreply extends Model
{
    // 表名
    protected $name = 'wechat_offiaccount_autoreply';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    public function getReplyContentAttr($value): string
    {
        return htmlspecialchars_decode($value);
    }

}