<?php

namespace app\admin\model\xmwechat\offiaccount;

use think\Model;

/**
 * Material
 */
class Material extends Model
{
    // 表名
    protected $name = 'wechat_offiaccount_material';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    protected $append = [
        'url',
        'title',
        'image',
    ];

    public function getContentAttr($value, $row): string
    {
        if ($row['type'] == 'text'){
            return !$value ? '' : htmlspecialchars_decode($value);
        }elseif ($row['type'] == 'link'){
            $data = json_decode($row['content'],true);
            return $data['content'];
        }else{
            return '';
        }

    }

    public function getTitleAttr($value, $row): string
    {
        if ($row['type'] == 'link'){
            $data = json_decode($row['content'],true);
            return $data['title'];
        }else{
            return '';
        }
    }

    public function getImageAttr($value, $row): string
    {
        if ($row['type'] == 'link'){
            $data = json_decode($row['content'],true);
            return $data['image'];
        }else{
            return '';
        }
    }
    public function getUrlAttr($value, $row): string
    {
        if ($row['type'] == 'link'){
            $data = json_decode($row['content'],true);
            return $data['url'];
        }else{
            return '';
        }
    }

}