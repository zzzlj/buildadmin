<?php

namespace app\admin\model\skin;

use think\Model;

/**
 * User
 */
class User extends Model
{
    // 表名
    protected $name = 'skin_user';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;


    public function apps(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(\app\admin\model\skin\Device::class, 'apps_id', 'id');
    }
}