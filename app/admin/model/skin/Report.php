<?php

namespace app\admin\model\skin;

use think\Model;

/**
 * Report
 */
class Report extends Model
{
    // 表名
    protected $name = 'skin_report';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


    public function apps(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(\app\admin\model\skin\Device::class, 'apps_id', 'id');
    }

    public function user(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(\app\admin\model\skin\User::class, 'user_id', 'id');
    }
}