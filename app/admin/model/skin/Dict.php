<?php

namespace app\admin\model\skin;

use think\Model;

/**
 * Dict
 */
class Dict extends Model
{
    // 表名
    protected $name = 'skin_dict';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}