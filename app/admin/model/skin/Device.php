<?php

namespace app\admin\model\skin;

use think\Model;

/**
 * Device
 */
class Device extends Model
{
    // 表名
    protected $name = 'skin_device';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}