<?php

namespace app\admin\model\skin\user;

use think\Model;

/**
 * File
 */
class File extends Model
{
    // 表名
    protected $name = 'skin_user_file';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}