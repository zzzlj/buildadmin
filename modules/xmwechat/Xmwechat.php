<?php

namespace modules\xmwechat;

use app\common\library\Menu;
use app\common\model\Config;
use think\facade\Cache;

class Xmwechat
{
    private string $uid = 'xmwechat';

    public function install(): void
    {
        $this->createNewMenus();
    }

    public function uninstall(): void
    {
        Menu::delete('xmwechat', true);
    }

    public function enable(): void
    {
        Menu::enable('xmwechat');
        Config::addQuickEntrance('公众号配置', '/admin/xmwechat/offiaccount/config');
        Config::addQuickEntrance('小程序配置', '/admin/xmwechat/miniprogram/config');
        Config::addQuickEntrance('支付配置', '/admin/xmwechat/payment/config');

        // 恢复配置
        $config = Cache::pull('xmwechat-module-config');
        if ($config) {
            @file_put_contents(config_path() . 'xmwechat.php', $config);
        }
    }

    public function disable(): void
    {
        Menu::disable('xmwechat');
        Config::removeQuickEntrance('公众号配置');
        Config::removeQuickEntrance('小程序配置');
        Config::removeQuickEntrance('支付配置');

        // 备份配置
        $config = @file_get_contents(config_path() . 'xmwechat.php');
        if ($config) {
            Cache::set('xmwechat-module-config', $config, 3600);
        }
    }

    public function update(): void
    {

    }


    public function createNewMenus(): void
    {
        $newMenus = [
            [
                'type'     => 'menu_dir',
                'title'    => '微信管理',
                'name'     => 'xmwechat',
                'path'     => 'xmwechat',
                'icon'     => 'fa fa-wechat',
                'weigh'    => '86',
                'pid'      => 0,
                'children' => [
                    [
                        'type'      => 'menu_dir',
                        'title'     => '公众号',
                        'name'      => 'xmwechat/offiaccount',
                        'path'      => 'xmwechat/offiaccount',
                        'icon'      => 'fa fa-tablet',
                        'menu_type' => 'tab',
                        'weigh'     => '95',
                        'children'  => [
                            [
                                'type'      => 'menu',
                                'title'     => '配置管理',
                                'name'      => 'xmwechat/offiaccount/config',
                                'path'      => 'xmwechat/offiaccount/config',
                                'icon'      => 'fa fa-gear',
                                'menu_type' => 'tab',
                                'component' => '/src/views/backend/xmwechat/offiaccount/config/index.vue',
                                'weigh'     => '90',
                                'children'  => [
                                    ['type' => 'button', 'title' => '查看', 'name' => 'xmwechat/offiaccount/config/getConfig'],
                                    ['type' => 'button', 'title' => '保存', 'name' => 'xmwechat/offiaccount/config/saveConfig'],
                                ],
                            ],
                            [
                                'type'      => 'menu',
                                'title'     => '菜单设置',
                                'name'      => 'xmwechat/offiaccount/menu',
                                'path'      => 'xmwechat/offiaccount/menu',
                                'icon'      => 'fa fa-th-large',
                                'menu_type' => 'tab',
                                'component' => '/src/views/backend/xmwechat/offiaccount/menu/index.vue',
                                'weigh'     => '80',
                                'children'  => [
                                    ['type' => 'button', 'title' => '查看', 'name' => 'xmwechat/offiaccount/menu/getMenu'],
                                    ['type' => 'button', 'title' => '保存并发布', 'name' => 'xmwechat/offiaccount/menu/saveMenu'],
                                ],
                            ],
                            [
                                'type'      => 'menu',
                                'title'     => '回复管理',
                                'name'      => 'xmwechat/offiaccount/autoreply',
                                'path'      => 'xmwechat/offiaccount/autoreply',
                                'icon'      => 'fa fa-commenting',
                                'menu_type' => 'tab',
                                'component' => '/src/views/backend/xmwechat/offiaccount/autoreply/index.vue',
                                'weigh'     => '70',
                                'children'  => [
                                    ['type' => 'button', 'title' => '查看', 'name' => 'xmwechat/offiaccount/autoreply/index'],
                                    ['type' => 'button', 'title' => '添加', 'name' => 'xmwechat/offiaccount/autoreply/add'],
                                    ['type' => 'button', 'title' => '编辑', 'name' => 'xmwechat/offiaccount/autoreply/edit'],
                                    ['type' => 'button', 'title' => '删除', 'name' => 'xmwechat/offiaccount/autoreply/del'],
                                ],
                            ],
                            [
                                'type'      => 'menu',
                                'title'     => '素材管理',
                                'name'      => 'xmwechat/offiaccount/material',
                                'path'      => 'xmwechat/offiaccount/material',
                                'icon'      => 'fa fa-folder',
                                'menu_type' => 'tab',
                                'component' => '/src/views/backend/xmwechat/offiaccount/material/index.vue',
                                'weigh'     => '60',
                                'children'  => [
                                    ['type' => 'button', 'title' => '查看', 'name' => 'xmwechat/offiaccount/material/index'],
                                    ['type' => 'button', 'title' => '添加', 'name' => 'xmwechat/offiaccount/material/add'],
                                    ['type' => 'button', 'title' => '编辑', 'name' => 'xmwechat/offiaccount/material/edit'],
                                    ['type' => 'button', 'title' => '删除', 'name' => 'xmwechat/offiaccount/material/del'],
                                ],
                            ],
                        ]
                    ],
                    [
                        'type'      => 'menu_dir',
                        'title'     => '小程序',
                        'name'      => 'xmwechat/miniprogram',
                        'path'      => 'xmwechat/miniprogram',
                        'icon'      => 'fa fa-tablet',
                        'menu_type' => 'tab',
                        'weigh'     => '80',
                        'children'  => [
                            [
                                'type'      => 'menu',
                                'title'     => '配置管理',
                                'name'      => 'xmwechat/miniprogram/config',
                                'path'      => 'xmwechat/miniprogram/config',
                                'icon'      => 'fa fa-gear',
                                'menu_type' => 'tab',
                                'component' => '/src/views/backend/xmwechat/miniprogram/config/index.vue',
                                'weigh'     => '90',
                                'children'  => [
                                    ['type' => 'button', 'title' => '查看', 'name' => 'xmwechat/miniprogram/config/getConfig'],
                                    ['type' => 'button', 'title' => '保存', 'name' => 'xmwechat/miniprogram/config/saveConfig'],
                                ]
                            ],
                        ],
                    ],
                    [
                        'type'      => 'menu',
                        'title'     => '支付配置',
                        'name'      => 'xmwechat/payment/config',
                        'path'      => 'xmwechat/payment/config',
                        'icon'      => 'fa fa-gear',
                        'menu_type' => 'tab',
                        'component' => '/src/views/backend/xmwechat/payment/config/index.vue',
                        'weigh'     => '70',
                        'children'  => [
                            ['type' => 'button', 'title' => '查看', 'name' => 'xmwechat/payment/config/getConfig'],
                            ['type' => 'button', 'title' => '保存', 'name' => 'xmwechat/payment/config/saveConfig'],

                        ],
                    ]
                ]
            ]
        ];
        Menu::create($newMenus, 'xmwechat');
    }
}