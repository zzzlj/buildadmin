SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for __PREFIX__wechat_offiaccount_material
-- ----------------------------
DROP TABLE IF EXISTS `__PREFIX__wechat_offiaccount_material`;
CREATE TABLE `__PREFIX__wechat_offiaccount_material` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
      `event_key` varchar(100) DEFAULT '' COMMENT '事件Key',
      `type` enum('image','video','voice','news','text') DEFAULT 'text' COMMENT '类型',
      `content` text COMMENT '内容',
      `create_time` bigint(20) unsigned DEFAULT NULL COMMENT '创建时间',
      `update_time` bigint(20) unsigned DEFAULT NULL COMMENT '更新时间',
      PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='微信公众号素材';

-- ----------------------------
-- Table structure for __PREFIX__wechat_offiaccount_autoreply
-- ----------------------------
DROP TABLE IF EXISTS `__PREFIX__wechat_offiaccount_autoreply`;
CREATE TABLE `__PREFIX__wechat_offiaccount_autoreply` (
       `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
       `keyword` varchar(100) DEFAULT '' COMMENT '关键词',
       `type` enum('follow','keyword','default') DEFAULT 'keyword' COMMENT '类型：follow=关注回复；keyword=关键词回复；invalidword=无效词回复',
       `msg_type` enum('text','image','voice','video','news') DEFAULT 'text' COMMENT '消息类型：text=文本；image=图片；voice=语音；news=图文',
       `status` enum('0','1') DEFAULT '0' COMMENT '状态:0=禁用;1=启用',
       `reply_content` text COMMENT '回复内容',
       `create_time` bigint(20) unsigned DEFAULT NULL COMMENT '创建时间',
       `update_time` bigint(20) unsigned DEFAULT NULL COMMENT '更新时间',
       PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='公众号自动回复';

-- ----------------------------
-- Records of __PREFIX__wechat_offiaccount_autoreply
-- ----------------------------
BEGIN;
INSERT INTO `__PREFIX__wechat_offiaccount_autoreply` (`keyword`, `type`, `msg_type`, `status`, `reply_content`, `create_time`, `update_time`) VALUES ('你好', 'keyword', 'text', '1', '你好呀', 1692594168, 1693809641);
INSERT INTO `__PREFIX__wechat_offiaccount_autoreply` (`keyword`, `type`, `msg_type`, `status`, `reply_content`, `create_time`, `update_time`) VALUES ('', 'follow', 'text', '1', '&lt;a href=&quot;https://buildadmin.com&quot;&gt;BuildAdmin&lt;/a&gt;\n\n欢迎使用流行技术栈快速创建商业级后台管理系统', 1692596134, 1693979021);
INSERT INTO `__PREFIX__wechat_offiaccount_autoreply` (`keyword`, `type`, `msg_type`, `status`, `reply_content`, `create_time`, `update_time`) VALUES ('', 'default', 'text', '1', '你好，这是默认回复哦～', 1693467221, 1693467221);
INSERT INTO `__PREFIX__wechat_offiaccount_autoreply` (`keyword`, `type`, `msg_type`, `status`, `reply_content`, `create_time`, `update_time`) VALUES ('哈哈', 'keyword', 'text', '1', '哈哈哈哈哈哈哈哈哈', 1693975726, 1693975726);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;