<?php
return [
    //公众号
    'offiAccount'    => [
        'app_id'                    => '',
        'secret'                    => '',
        'redirect_url'              => 'http://localhost:8000/index.php/api/xmwechat.Offiaccount/oauth',
        'offi_name'                 => '',
        'original_id'               => '',
        'offi_qr_code'              => '',
        'server_url'                => request()->domain() . '/index.php/api/xmwechat.Offiaccount/messageServe',
        'server_token'              => '',
        'encoding_aes_key'          => '',
        'message_encryption_method' => 'plaintext',
        'business_domain'           => request()->domain(),
        'js_security_domain'        => request()->domain(),
        'website_auth_domain'       => request()->domain(),
    ],

    //小程序
    'miniProgram'    => [
        'app_id'                    => '',
        'secret'                    => '',
        'name'                      => '',
        'original_id'               => '',
        'qr_code'                   => '',
        'request_domain'            => request()->domain(),
        'socket_domain'             => 'wss://' . str_replace(['http://', 'https://'], '', request()->domain()),
        'upload_file_domain'        => request()->domain(),
        'download_file_domain'      => request()->domain(),
        'udp_domain'                => 'udp://' . str_replace(['http://', 'https://'], '', request()->domain()),
        'tcp_domain'                => 'tcp://' . str_replace(['http://', 'https://'], '', request()->domain()),
        'server_url'                => request()->domain() . '/index.php/api/xmwechat.Miniprogram/messageServe',
        'server_token'              => 'TOKEN',
        'encoding_aes_key'          => '',
        'message_encryption_method' => 'plaintext',
    ],

    //微信支付(v3)
    'payment'        => [
        'mch_id'      => '',
        'secret_key'  => 'secretkey',
        'notify_url'  => 'http://localhost:8000/index.php/api/xmwechat.Payment/notify',
        'certificate' => 'app/cert/apiclient_cert.pem',
        'private_key' => 'app/cert/apiclient_key.pem',
    ],

    // 合作伙伴（服务商）模式支付（v3）
    'partnerPayment' => [
        'sp_mchid'           => '',
        'sp_appid'           => '',
        'sub_mchid'          => '',
        'secret_key'         => 'partnersecretkey',
        'notify_url'         => 'http://localhost:8000/index.php/api/xmwechat.PartnerPayment/payNotify',
        'refunds_notify_url' => 'http://localhost:8000/index.php/api/xmwechat.PartnerPayment/refundsNotify',
        'certificate'        => 'app/cert/partner/apiclient_cert.pem',
        'private_key'        => 'app/cert/partner/apiclient_cert.pem',
    ],

];